import os

# Rename the subtitle file to txt file
for f_name in os.listdir('C:/Users/dkver/Desktop/Angular 8 - Tutorials'):
        if f_name.endswith('.srt'):
                os.rename(f_name, 'sub.txt')

# Read the lines and turn them into paragraph
file1 = open("sub.txt","r")
lines = file1.readlines()
final = []
i = 2
while i <= len(lines):
	line = lines[i].rstrip('\n')
	final.append(line)
	i += 4	
file1.close()
subs = " ".join(final)

# Create new file for storing paragraph
sub_final = ''
sub_final_index = 1
for f_name in os.listdir('C:/Users/dkver/Desktop/Angular 8 - Tutorials'):
        if f_name.endswith('.txt'):
                index = int(f_name[0:-4])
                if index > sub_final_index:
                        sub_final_index = index
        else:
                break

sub_final_index += 1
sub_final = str(sub_final_index) + '.txt'
file2 = open(sub_final, "w")
print(sub_final + ' created')
file2.write(subs)
file2.close()
os.remove("sub.txt")
os.listdir()
